package com.que5;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * @author zhouminghua
 * @version 1.0
 * @date 2025-2-15 09:28:57
 * @description 启动类
 * @site https://www.que5.com
 */
@SpringBootApplication
@EnableDiscoveryClient
public class MyGatewayAppApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(MyGatewayAppApplication.class);

        ConfigurableApplicationContext configurableApplicationContext = app.run(args);
        Environment env = configurableApplicationContext.getEnvironment();
        app.setBannerMode(Banner.Mode.CONSOLE);
    }
}
