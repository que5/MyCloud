package com.que5;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * <p>
 * 用户列表 前端控制器
 * </p>
 *
 * @author evan
 * @since 2024-11-28
 */
@RestController
@RequestMapping("/cloud")
public class MyApp1Controller {

    @Value("${server.port}")
    private Integer port;
    @Value("${spring.application.name}")
    private String name;
    @GetMapping("/hello")
    public String hello(@RequestParam(name = "msg",required = false) String msg) {
        return "hello name:"+name+"，port:"+port + " msg:" + msg;
    }

    @Resource
    RestTemplate restTemplate;
    @GetMapping("/getApp2")
    public String getApp2() {

        return "getApp2:"+restTemplate.getForObject("http://myapp2/cloud/hello?msg=我来自myapp1",String.class);
    }
}
