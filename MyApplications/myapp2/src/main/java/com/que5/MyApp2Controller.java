package com.que5;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户列表 前端控制器
 * </p>
 *
 * @author evan
 * @since 2024-11-28
 */
@RestController
@RequestMapping("/cloud")
public class MyApp2Controller {

    @Value("${server.port}")
    private Integer port;
    @Value("${spring.application.name}")
    private String name;
    @GetMapping("/hello")
    public String hello(@RequestParam(name = "msg",required = false) String msg) {
        return "hello name:"+name+"，port:"+port + " msg:" + msg;
    }
}
